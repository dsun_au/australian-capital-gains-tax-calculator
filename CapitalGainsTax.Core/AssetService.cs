﻿using CapitalGainsTax.Core.Models;
using System.Collections.ObjectModel;

namespace CapitalGainsTax.Core;

public class AssetService
{
    #region Non-public Fields

    protected ObservableCollection<Portfolio> portfolios = new();

    #endregion

    #region Public Properties

    public ReadOnlyObservableCollection<Portfolio> Portfolios => new(portfolios);

    #endregion

    #region Public Methods

    public void CreatePortfolio()
    {
        throw new NotImplementedException();
    }

    public void RemovePortfolio()
    {
        throw new NotImplementedException();
    }

    #endregion
}
