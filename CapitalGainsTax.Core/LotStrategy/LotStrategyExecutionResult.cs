﻿using CapitalGainsTax.Core.Models;

namespace CapitalGainsTax.Core.LotStrategy;

/// <summary>
/// Results of an execution of a sell of tax lots.
/// </summary>
public record LotStrategyExecutionResult
{
    public AssetState ResultantState => LotTransactions.OrderBy(transaction => transaction.Time).Last().ResultantState;
    public IReadOnlyCollection<LotTransaction> LotTransactions { get; }
    public CapitalGainEvent TotalCapitalGainEvent { get; }

    public LotStrategyExecutionResult(CapitalGainEvent TotalCapitalGainEvent, IReadOnlyCollection<LotTransaction> LotTransactions)
    {
        this.LotTransactions = LotTransactions;
        this.TotalCapitalGainEvent = TotalCapitalGainEvent;
    }
}