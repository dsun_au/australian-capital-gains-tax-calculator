﻿using CapitalGainsTax.Core.Models;

namespace CapitalGainsTax.Core.LotStrategy;

/// <summary>
/// Handler which will execute a sell on given tax lots with a particular strategy.
/// </summary>
public interface ILotStrategyHandler
{
    /// <summary>
    /// Execute the sell on tax lots.
    /// </summary>
    /// <param name="time">Time of sell transaction.</param>
    /// <param name="quantity">Quantity of assets to sell.</param>
    /// <param name="unitPrice">Unit price assets sold at.</param>
    /// <param name="taxLots">The tax lots to sell from.</param>
    /// <returns>The resultant remaining tax lots, the individual lot sell transactions, and the capital gains result.</returns>
    LotStrategyExecutionResult Execute(DateTimeOffset time, decimal quantity, decimal unitPrice, params TaxLot[] taxLots);
}

