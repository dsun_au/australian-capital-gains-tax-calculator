﻿using CapitalGainsTax.Core.Models;
using CommunityToolkit.Diagnostics;

namespace CapitalGainsTax.Core.LotStrategy;

/// <summary>
/// <para>Executes a sell on the given tax lots using the first-in first-out (FIFO) strategy.</para>
/// <para>The FIFO strategy sells the oldest assets first.</para>
/// </summary>
public class FIFOLotStrategyHandler : ILotStrategyHandler
{
    public LotStrategyExecutionResult Execute(DateTimeOffset time, decimal quantity, decimal unitPrice, params TaxLot[] taxLots)
    {
        Guard.IsGreaterThan(taxLots.Length, 0, nameof(taxLots));

        IOrderedEnumerable<TaxLot> initialTaxLots = taxLots.OrderBy(lot => lot.Time);

        decimal totalShortTermGains = 0;
        decimal totalLongTermGains = 0;
        decimal totalLoss = 0;

        AssetState intermediateInitialState = new(time, initialTaxLots.ToList());
        List<LotTransaction> lotTransactions = new();

        decimal totalQuantityToSellRemaining = quantity;

        foreach (TaxLot taxLotToSell in initialTaxLots)
        {
            decimal quantityToSellFromThisLot = CoerceSellQuantityWithinLimit(totalQuantityToSellRemaining, taxLotToSell.Quantity);

            LotTransaction intermediateTransaction = new(taxLotToSell, time, quantityToSellFromThisLot, unitPrice, intermediateInitialState);
            lotTransactions.Add(intermediateTransaction);

            TabulateCapitalGains(intermediateTransaction.CapitalGainEvent!);

            // Prepare for next loop
            totalQuantityToSellRemaining -= quantityToSellFromThisLot;
            intermediateInitialState = intermediateTransaction.ResultantState;

            if (totalQuantityToSellRemaining == 0)
                break;
        }

        CapitalGainEvent totalCapitalGainsEvent = new(time, totalShortTermGains, totalLongTermGains, totalLoss);

        return new LotStrategyExecutionResult(totalCapitalGainsEvent, lotTransactions);

        #region Local Methods

        static decimal CoerceSellQuantityWithinLimit(decimal sellQuantity, decimal limit)
        {
            return sellQuantity >= limit ? limit : sellQuantity;
        }

        void TabulateCapitalGains(CapitalGainEvent capitalGainEvent)
        {
            totalLoss += capitalGainEvent.Loss;
            totalLongTermGains += capitalGainEvent.LongTermGain;
            totalShortTermGains += capitalGainEvent.ShortTermGain;
        }

        #endregion
    }
}

