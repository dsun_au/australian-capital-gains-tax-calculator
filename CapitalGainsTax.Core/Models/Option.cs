﻿using CapitalGainsTax.Core.LotStrategy;

namespace CapitalGainsTax.Core.Models;

public enum OptionType
{
    Call,
    Put
}

public class Option : Security
{
    #region Public Properties

    public OptionType OptionType { get; }

    public double StrikePrice { get; } = 0;

    public DateTimeOffset Expiry { get; } = default;

    #endregion

    public Option(string identifier, BuyTransaction buyTransaction, ILotStrategyHandler lotTransactionHandler)
        : base(identifier, buyTransaction, lotTransactionHandler)
    {

    }
}
