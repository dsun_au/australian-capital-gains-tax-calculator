﻿using System.Collections.ObjectModel;
using CapitalGainsTax.Core.LotStrategy;

namespace CapitalGainsTax.Core.Models;

public class Asset : ModelBase, IAsset
{
    #region Non-public Fields

    protected ObservableCollection<ITransaction> transactions = new();

    #endregion

    #region Public Properties

    public string Identifier { get; }

    public ReadOnlyObservableCollection<ITransaction> Transactions => new(transactions);

    public AssetState CurrentState => transactions?.OrderBy(transaction => transaction.Time).Last().ResultantState ??
                                        throw new InvalidOperationException("No transactions found.");

    public ILotStrategyHandler LotStrategyHandler { get => _LotStrategyHandler; protected set => SetProperty(ref _LotStrategyHandler, value); }

    #region Backing Fields

    private ILotStrategyHandler _LotStrategyHandler = null!;

    #endregion

    #endregion

    #region Constructors

    public Asset(string identifier, BuyTransaction firstTransaction, ILotStrategyHandler lotStrategyHandler)
    {
        Identifier = identifier;
        transactions.Add(firstTransaction);
        LotStrategyHandler = lotStrategyHandler;
    }

    #endregion

    #region Non-public Methods

    protected void RegenerateTaxLotTransactions()
    {
        for (int i = 0; i < transactions.Count; i++)
        {
            if (i > 0 && transactions[i] is SellTransaction sellTransaction)
                sellTransaction.RegenerateResultantState(transactions[i - 1].ResultantState!, LotStrategyHandler);
        }
    }

    #endregion

    #region Public Methods

    public void AddTransaction(ITransaction transaction)
    {
        transactions.Add(transaction);
    }

    public void ChangeTaxLotStrategy(ILotStrategyHandler lotStrategyHandler)
    {
        LotStrategyHandler = lotStrategyHandler;
        RegenerateTaxLotTransactions();
    }

    #endregion

}