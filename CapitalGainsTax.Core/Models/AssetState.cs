﻿using System.Collections.ObjectModel;

namespace CapitalGainsTax.Core.Models;

public record AssetState : StateBase
{
    #region Public Properties

    public IReadOnlyList<TaxLot> TaxLots { get; init; } 

    public decimal TotalQuantity => TaxLots.Sum(taxLot => taxLot.Quantity);

    public decimal TotalValue => TaxLots.Sum(taxLot => taxLot.BaseCost);

    public decimal AverageUnitPrice => TotalValue / TotalQuantity;

    #endregion

    #region Constructors

    internal AssetState(DateTimeOffset Time, IList<TaxLot> TaxLots) : base(Time)
    {
        this.TaxLots = new ReadOnlyCollection<TaxLot>(TaxLots);
    }

    #endregion
}