﻿namespace CapitalGainsTax.Core.Models;

public class BuyTransaction : TransactionBase
{
    internal BuyTransaction(DateTimeOffset time, decimal quantity, decimal unitPrice, AssetState initialState,
                            decimal fee = 0, decimal exchangeRate = 1, CapitalGainEvent? capitalGainEvent = null)
        : base(time, quantity, unitPrice, initialState, fee, exchangeRate, capitalGainEvent)
    {

    }

    protected override AssetState CalculateResultantState(AssetState initialState)
    {
        List<TaxLot> resultantTaxLots = new(initialState.TaxLots);
        IEnumerable<TaxLot> existingMatchingLots = initialState.TaxLots.Where(lot => lot.Time.Equals(Time) && lot.UnitPrice.Equals(UnitPrice));
        
        if (existingMatchingLots.Count() > 1)
            throw new InvalidOperationException("Unexpectedly found a repeated tax lot.");
        else if (existingMatchingLots.Count() == 1)
        {
            TaxLot existingMatchingLot = existingMatchingLots.Single();
  
            resultantTaxLots.Remove(existingMatchingLot);
            resultantTaxLots.Add(new TaxLot(existingMatchingLot.Time, existingMatchingLot.Quantity + Quantity, existingMatchingLot.UnitPrice));
        }
        else
            resultantTaxLots.Add(new TaxLot(Time, Quantity, UnitPrice));

        return new AssetState(Time, resultantTaxLots.ToArray());
    }
}