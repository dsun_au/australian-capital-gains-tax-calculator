﻿using CapitalGainsTax.Core.LotStrategy;

namespace CapitalGainsTax.Core.Models;

public class CryptoCurrency : Asset
{
    public CryptoCurrency(string identifier, BuyTransaction buyTransaction, ILotStrategyHandler lotTransactionHandler)
        : base(identifier, buyTransaction, lotTransactionHandler)
    {

    }
}