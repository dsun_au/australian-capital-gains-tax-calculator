﻿namespace CapitalGainsTax.Core.Models;

public interface ITransaction
{
    /// <summary>
    /// Time of this transaction.
    /// </summary>
    DateTimeOffset Time { get; }

    /// <summary>
    /// The quantity of assets in this transation.
    /// </summary>
    decimal Quantity { get; }

    /// <summary>
    /// The unit price of the asset in this transaction.
    /// </summary>
    decimal UnitPrice { get; }

    /// <summary>
    /// Fee that is incurred by this transaction.
    /// </summary>
    decimal Fee { get; }

    /// <summary>
    /// Exchange rate at the time of transaction from asset currency to base currency.
    /// </summary>
    decimal ExchangeRate { get; }

    /// <summary>
    /// The state of the asset as a result of this transaction, if any.
    /// </summary>
    AssetState? ResultantState { get; }

    /// <summary>
    /// The capital gains event as a result of this transaction, if any.
    /// </summary>
    CapitalGainEvent? CapitalGainEvent { get; }

    /// <summary>
    /// Regenerate the <see cref="ResultantState"/> of this transaction based off <paramref name="initialState"/>.
    /// </summary>
    /// <param name="initialState">The <see cref="AssetState"/> before this transaction is executed.</param>
    void RegenerateResultantState(AssetState initialState);
}