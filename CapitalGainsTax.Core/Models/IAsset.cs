﻿using System.Collections.ObjectModel;
using CapitalGainsTax.Core.LotStrategy;

namespace CapitalGainsTax.Core.Models;

public interface IAsset
{
    /// <summary>
    /// The unique identifier of this asset.
    /// </summary>
    string Identifier { get; }

    /// <summary>
    /// The current state of this asset.
    /// </summary>
    AssetState CurrentState { get; }

    /// <summary>
    /// The strategy of optimising tax lots for this asset.
    /// </summary>
    ILotStrategyHandler LotStrategyHandler { get; }

    /// <summary>
    /// All transactions of this asset.
    /// </summary>
    ReadOnlyObservableCollection<ITransaction> Transactions { get; }

    /// <summary>
    /// Change to a different tax lot strategy.
    /// </summary>
    /// <param name="taxLotStrategy">The tax lot strategy.</param>
    void ChangeTaxLotStrategy(ILotStrategyHandler lotStrategyHandler);
}