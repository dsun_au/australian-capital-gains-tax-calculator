﻿using CommunityToolkit.Diagnostics;

namespace CapitalGainsTax.Core.Models;

/// <summary>
/// Base class transactions are derived from.
/// </summary>
public abstract class TransactionBase : ModelBase, ITransaction
{
    #region Public Properties

    public DateTimeOffset Time { get; }
    public decimal Quantity { get; }
    public decimal UnitPrice { get; }
    public decimal Fee { get; }
    public decimal ExchangeRate { get; }
    public AssetState ResultantState { get => _assetState; protected set => SetProperty(ref _assetState, value); }
    public CapitalGainEvent? CapitalGainEvent { get => _capitalGainEvent; protected set => SetProperty(ref _capitalGainEvent, value); }

    #region Backing Fields

    private AssetState _assetState = null!;
    private CapitalGainEvent? _capitalGainEvent = null;

    #endregion

    #endregion

    #region Constructors

    /// <param name="time">Time the transaction took place.</param>
    /// <param name="quantity">The quantity of assets involved in the transaction.</param>
    /// <param name="unitPrice">The price of each unit of asset in this transaction.</param>
    /// <param name="fee">Additional fees associated with this transaction.</param>
    /// <param name="exchangeRate">Exchange rate from base currency (AUD) to transaction currency.</param>
    /// <param name="initialState"></param>
    /// <param name="capitalGainEvent"></param>
    /// <param name="generateAfterStateAutomatically">
    /// <para>Controls whether the constructor of <see cref="TransactionBase"/> will call <see cref="RegenerateResultantState(AssetState)"/>
    /// in the constructor.</para>
    /// <para>Set to <c>false</c> and call <see cref="RegenerateResultantState(AssetState)"/> in the derived class constructor if
    /// <see cref="CalculateResultantState(AssetState)"/> needs the derived class constructor to run first.</para>
    /// </param>
    /// <inheritdoc cref="TransactionBase"/>
    internal TransactionBase(DateTimeOffset time, decimal quantity, decimal unitPrice, AssetState initialState, decimal fee = 0,
                            decimal exchangeRate = 1, CapitalGainEvent? capitalGainEvent = null, bool generateAfterStateAutomatically = true)
    {
        Time = time;
        Quantity = quantity;
        UnitPrice = unitPrice;
        Fee = fee;
        ExchangeRate = exchangeRate;
        CapitalGainEvent = capitalGainEvent;

        if(generateAfterStateAutomatically)
            RegenerateResultantState(initialState);
    }

    #endregion

    public void RegenerateResultantState(AssetState initialState)
    {
        ResultantState = CalculateResultantState(initialState);
    }

    /// <summary>
    /// Calculate the resultant asset state of this transaction given the incoming state.
    /// </summary>
    /// <param name="initialState">Asset state just before this transaction is executed.</param>
    /// <returns>The <see cref="AssetState"/> as a result of the execution of this transaction.</returns>
    protected abstract AssetState CalculateResultantState(AssetState initialState);
}