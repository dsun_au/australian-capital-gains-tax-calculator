﻿using CapitalGainsTax.Core.LotStrategy;

namespace CapitalGainsTax.Core.Models;

public class Stock : Security
{
    public Stock(string identifier, BuyTransaction buyTransaction, ILotStrategyHandler lotTransactionHandler)
        : base(identifier, buyTransaction, lotTransactionHandler)
    {

    }
}