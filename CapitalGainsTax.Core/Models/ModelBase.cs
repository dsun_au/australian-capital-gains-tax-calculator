﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CapitalGainsTax.Core.Models;

/// <summary>
/// Base class that implements <see cref="INotifyPropertyChanged"/> and <see cref="INotifyPropertyChanging"/>.
/// </summary>
public abstract class ModelBase : INotifyPropertyChanged, INotifyPropertyChanging
{
    protected void SetProperty<TProperty>(ref TProperty backingField, TProperty value, [CallerMemberName] string propertyName = "")
    {
        RaisePropertyChanging(propertyName);
        backingField = value;
        RaisePropertyChanged(propertyName);
    }

    protected void RaisePropertyChanged([CallerMemberName] string propertyName = "")
    {
        if (string.IsNullOrEmpty(propertyName)) throw new ArgumentException("Cannot be empty.", nameof(propertyName));

        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    protected void RaisePropertyChanging([CallerMemberName] string propertyName = "")
    {
        if (string.IsNullOrEmpty(propertyName)) throw new ArgumentException("Cannot be empty.", nameof(propertyName));

        PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
    }

    public event PropertyChangedEventHandler? PropertyChanged;
    public event PropertyChangingEventHandler? PropertyChanging;
}