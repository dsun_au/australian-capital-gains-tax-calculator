﻿namespace CapitalGainsTax.Core.Models;

public abstract record StateBase(DateTimeOffset Time);