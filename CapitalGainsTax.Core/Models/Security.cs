﻿using CapitalGainsTax.Core.LotStrategy;

namespace CapitalGainsTax.Core.Models;

public abstract class Security : Asset
{
    #region Public Properties

    public string Exchange { get; } = string.Empty;

    public string Currency { get; } = string.Empty;

    #endregion

    public Security(string identifier, BuyTransaction buyTransaction, ILotStrategyHandler lotTransactionHandler) 
        : base(identifier, buyTransaction, lotTransactionHandler)
    {

    }
}
