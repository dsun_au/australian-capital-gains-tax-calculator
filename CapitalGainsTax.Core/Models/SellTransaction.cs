﻿using System.Collections.ObjectModel;
using CapitalGainsTax.Core.LotStrategy;

namespace CapitalGainsTax.Core.Models;

public class SellTransaction : TransactionBase
{
    #region Non-public Fields

    protected readonly ObservableCollection<LotTransaction> lotTransactions = new();

    protected ILotStrategyHandler lotStrategyHandler;

    #endregion

    #region Public Properties

    public ReadOnlyObservableCollection<LotTransaction> LotTransactions => new(lotTransactions);

    #endregion

    #region Contructors

    /// <param name="initialState">The incoming <see cref="AssetState"/> that this transaction works off.</param>
    /// <inheritdoc cref="TransactionBase"/>
    internal SellTransaction(AssetState initialState, ILotStrategyHandler lotStrategyHandler, DateTimeOffset time, decimal quantity,
        decimal unitPrice, decimal fee = 0, decimal exchangeRate = 1)
        : base(time, quantity, unitPrice, initialState, fee, exchangeRate, generateAfterStateAutomatically: false)
    {
        this.lotStrategyHandler = lotStrategyHandler;
        RegenerateResultantState(initialState);
    }

    #endregion

    #region Non-public Methods

    internal void RegenerateResultantState(AssetState initialState, ILotStrategyHandler lotStrategyHandler)
    {
        this.lotStrategyHandler = lotStrategyHandler;
        RegenerateResultantState(initialState);
    }

    protected override AssetState CalculateResultantState(AssetState initialState)
    {
        LotStrategyExecutionResult result = lotStrategyHandler.Execute(Time, Quantity, UnitPrice, initialState.TaxLots.ToArray());

        foreach (LotTransaction lotTransaction in result.LotTransactions)
            lotTransactions.Add(lotTransaction);

        CapitalGainEvent = result.TotalCapitalGainEvent;

        return LotTransactions.OrderBy(transaction => transaction.Time).Last().ResultantState;
    }

    #endregion
}

