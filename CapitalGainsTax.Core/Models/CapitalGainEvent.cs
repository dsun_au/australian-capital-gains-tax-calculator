﻿namespace CapitalGainsTax.Core.Models;

public record CapitalGainEvent
{

    #region Public Properties

    public DateTimeOffset Time { get; }

    public decimal ShortTermGain { get; } = 0;

    public decimal LongTermGain { get; } = 0;

    public decimal Loss { get; } = 0;

    public decimal NetGain => ShortTermGain + LongTermGain - Loss;


    #region Backing Fields   

    #endregion

    #endregion

    #region Constructors

    internal CapitalGainEvent(DateTimeOffset Time, decimal NetGain, bool IsLongTerm)
    {
        this.Time = Time;

        if (NetGain < 0)
            Loss += Math.Abs(NetGain);
        else if (IsLongTerm)
            LongTermGain += NetGain;
        else
            ShortTermGain += NetGain;
    }

    internal CapitalGainEvent(DateTimeOffset Time, decimal ShortTermGain, decimal LongTermGain, decimal Loss)
    {
        this.Time = Time;
        this.ShortTermGain = ShortTermGain;
        this.LongTermGain = LongTermGain;
        this.Loss = Loss;
    }

    #endregion

    #region Public Methods

    #endregion
}