﻿namespace CapitalGainsTax.Core.Models;

public record TaxLot : StateBase
{
    #region Public Properties

    public decimal BaseCost => Quantity * UnitPrice;

    public decimal UnitPrice { get; init; }

    public decimal Quantity { get; init; }

    #endregion
    public TaxLot(DateTimeOffset Time, decimal Quantity, decimal UnitPrice) : base(Time)
    {
        this.UnitPrice = UnitPrice;
        this.Quantity = Quantity;
    }
}