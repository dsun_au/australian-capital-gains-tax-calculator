﻿using CommunityToolkit.Diagnostics;
using static CapitalGainsTax.Core.Utilities.Utilities;

namespace CapitalGainsTax.Core.Models;

public class LotTransaction : TransactionBase
{

    #region Public Properties

    public TaxLot LotToSellFrom { get; }

    #endregion

    internal LotTransaction(TaxLot lotToSellFrom, DateTimeOffset time, decimal quantity, decimal unitPrice, AssetState initialState, decimal exchangeRate = 1)
        : base(time, quantity, unitPrice, initialState, 0, exchangeRate, generateAfterStateAutomatically: false)
    {
        Guard.IsLessThanOrEqualTo(quantity, lotToSellFrom.Quantity, nameof(quantity));
        Guard.IsGreaterThanOrEqualTo(time, lotToSellFrom.Time, nameof(time));

        LotToSellFrom = lotToSellFrom;

        RegenerateResultantState(initialState);
    }

    protected override AssetState CalculateResultantState(AssetState initialState)
    {
        IReadOnlyList<TaxLot> initialTaxLots = initialState.TaxLots;
        List<TaxLot> afterTaxLots = new(initialTaxLots);
        afterTaxLots.Remove(LotToSellFrom);

        // Partial sale of a lot
        if (Quantity < LotToSellFrom.Quantity)
            afterTaxLots.Add(new TaxLot(LotToSellFrom.Time, LotToSellFrom.Quantity - Quantity, LotToSellFrom.UnitPrice));

        CapitalGainsResult capitalGainsResult = CalculateCapitalGains(Time, Quantity, UnitPrice, LotToSellFrom);
        CapitalGainEvent = new CapitalGainEvent(Time, capitalGainsResult.NetGain, capitalGainsResult.IsLongTerm);

        AssetState resultantState = new(Time, afterTaxLots.ToArray());

        return resultantState;
    }
}