﻿using System.Collections.ObjectModel;
using CapitalGainsTax.Core.LotStrategy;

namespace CapitalGainsTax.Core.Models;

public class Portfolio : ModelBase
{
    #region Non-public Fields

    protected readonly ObservableCollection<Asset> assets = new();

    #endregion

    #region Public Properties

    public ReadOnlyObservableCollection<Asset> Assets => new(assets);


    public ILotStrategyHandler LotStrategyHandler{ get => _lotStrategyHandler; protected set => SetProperty(ref _lotStrategyHandler, value); }

    public decimal TotalValue => throw new NotImplementedException();

    #region Backing Fields

    private ILotStrategyHandler _lotStrategyHandler = null!;

    #endregion

    #endregion

    #region Constructors

    public Portfolio(ILotStrategyHandler lotStrategyHandler)
    {
        LotStrategyHandler = lotStrategyHandler;
    }

    #endregion

    #region Public Methods

    public void ChangeTaxLotStrategy()
    {
        throw new NotImplementedException();
    }

    #endregion
}