﻿using CapitalGainsTax.Core.Models;

namespace CapitalGainsTax.Core.Utilities;

/// <summary>
/// Static class that contains miscellaneous methods.
/// </summary>
static internal class Utilities
{
    public const int DAYS_PER_YEAR = 365;
    public readonly static TimeSpan LONG_TERM_TIMESPAN = TimeSpan.FromDays(DAYS_PER_YEAR);

    static internal CapitalGainsResult CalculateCapitalGains(DateTimeOffset time, decimal quantity, decimal sellUnitPrice, TaxLot taxLotToSell)
    {
        if (quantity > taxLotToSell.Quantity) throw new ArgumentException("Quantity to sell is greater than available in the tax lot.");

        TimeSpan timeBetweenBuyAndSell = time.Subtract(taxLotToSell.Time);

        if (timeBetweenBuyAndSell < TimeSpan.Zero)
            throw new InvalidOperationException("Sell transaction is before asset acquisition time.");

        return new CapitalGainsResult(CalculateGain(), IsLongTerm(timeBetweenBuyAndSell));

        #region Local Methods

        decimal CalculateGain()
        {
            return (sellUnitPrice - taxLotToSell.UnitPrice) * quantity;
        }

        #endregion
    }

    /// <summary>
    /// Determines whether the <see cref="TimeSpan"/> <paramref name="timeBetweenBuyAndSell"/> is long enough to count as long term 
    /// capital gains.
    /// </summary>
    /// <param name="timeBetweenBuyAndSell">Time between the purchase and sale of the asset.</param>
    /// <returns><c>true</c> if <paramref name="timeBetweenBuyAndSell"/> counts as long term, otherwise <c>false</c>.</returns>
    static internal bool IsLongTerm(TimeSpan timeBetweenBuyAndSell)
    {
        return timeBetweenBuyAndSell > LONG_TERM_TIMESPAN;
    }

    /// <summary>
    /// Calculation result of <see cref="CalculateCapitalGains(decimal, decimal, TaxLot)"/>.
    /// </summary>
    internal record CapitalGainsResult(decimal NetGain, bool IsLongTerm);
}
