﻿using Microsoft.AspNetCore.Identity;

namespace CapitalGainsTax.BlazorApp.Server.Models;
public class ApplicationUser : IdentityUser
{
}
