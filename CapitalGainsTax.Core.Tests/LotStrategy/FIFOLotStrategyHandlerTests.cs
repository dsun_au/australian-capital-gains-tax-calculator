﻿using CapitalGainsTax.Core.Models;
using CapitalGainsTax.Core.LotStrategy;
using static CapitalGainsTax.Core.Tests.Asset;

namespace CapitalGainsTax.Core.Tests.LotStrategy;

public class FIFOLotStrategyHandlerTests
{
    private readonly TaxLot[] taxLots = new TaxLot[] { TaxLot1, TaxLot2, TaxLot3 };
    private readonly static DateTimeOffset sellTransactionTime = DateTimeOffset.Now;

    private readonly FIFOLotStrategyHandler FIFOLotStrategyHandler = new();

    public FIFOLotStrategyHandlerTests()
    {

    }

    [Fact]
    public void ExecuteFIFOSellAll()
    {
        decimal totalQuantity = TaxLot1.Quantity + TaxLot2.Quantity + TaxLot3.Quantity;

        LotStrategyExecutionResult lotStrategyResult = FIFOLotStrategyHandler.Execute(sellTransactionTime, totalQuantity, 5, taxLots);

        Assert.Empty(lotStrategyResult.ResultantState.TaxLots);

        Assert.Equal(3, lotStrategyResult.LotTransactions.Count);
    }

    [Fact]
    public void ExecuteFIFOSellPartOfOneLot()
    {
        decimal sellUnitPrice = 5;
        decimal sellQuantity = TaxLot1.Quantity / 4;
        decimal remainingQuantity = TaxLot1.Quantity - sellQuantity;

        decimal shortTermCapitalGains = (sellUnitPrice - TaxLot1.UnitPrice) * sellQuantity;

        LotStrategyExecutionResult lotStrategyResult = FIFOLotStrategyHandler.Execute(sellTransactionTime, sellQuantity, 5, taxLots);

        Assert.Equal(3, lotStrategyResult.ResultantState.TaxLots.Count);
        Assert.Single(lotStrategyResult.ResultantState.TaxLots, TaxLot2);
        Assert.Single(lotStrategyResult.ResultantState.TaxLots, TaxLot3);
        Assert.Single(lotStrategyResult.ResultantState.TaxLots, new TaxLot(TaxLot1.Time, remainingQuantity, TaxLot1.UnitPrice));

        Assert.Single(lotStrategyResult.LotTransactions);
        Assert.Single(lotStrategyResult.LotTransactions,
            lot => lot.Time == sellTransactionTime && lot.Quantity == sellQuantity && lot.UnitPrice == sellUnitPrice);

        Assert.Equal(shortTermCapitalGains, lotStrategyResult.TotalCapitalGainEvent.ShortTermGain);
        Assert.Equal(0, lotStrategyResult.TotalCapitalGainEvent.LongTermGain);
        Assert.Equal(0, lotStrategyResult.TotalCapitalGainEvent.Loss);
    }

    [Fact]
    public void ExecuteFIFOSellMoreThanOneLot()
    {
        TaxLot[] taxLots = new TaxLot[] { LongTermTaxLot1, TaxLot1, TaxLot2, TaxLot3 };
        decimal sellUnitPrice = 5;
        decimal taxLot1SellQuantity = TaxLot1.Quantity * 0.22m;
        decimal totalSellQuantity = LongTermTaxLot1.Quantity + taxLot1SellQuantity;
        decimal remainingTaxLot1Quantity = LongTermTaxLot1.Quantity + TaxLot1.Quantity - totalSellQuantity;

        decimal shortTermCapitalGains = (sellUnitPrice - TaxLot1.UnitPrice) * taxLot1SellQuantity;
        decimal longTermCapitalGains = (sellUnitPrice - LongTermTaxLot1.UnitPrice) * LongTermTaxLot1.Quantity;

        LotStrategyExecutionResult lotStrategyResult = FIFOLotStrategyHandler.Execute(sellTransactionTime, totalSellQuantity, sellUnitPrice, taxLots);

        // Check expected resultant tax lots
        Assert.Equal(3, lotStrategyResult.ResultantState.TaxLots.Count);
        Assert.Single(lotStrategyResult.ResultantState.TaxLots, TaxLot3);
        Assert.Single(lotStrategyResult.ResultantState.TaxLots, new TaxLot(TaxLot1.Time, remainingTaxLot1Quantity, TaxLot1.UnitPrice));

        // Check expected lot transactions
        Assert.Equal(2, lotStrategyResult.LotTransactions.Count());
        Assert.Single(lotStrategyResult.LotTransactions, 
            lot => lot.Time == sellTransactionTime && lot.Quantity == LongTermTaxLot1.Quantity && lot.UnitPrice == sellUnitPrice);
        Assert.Single(lotStrategyResult.LotTransactions,
            lot => lot.Time == sellTransactionTime && lot.Quantity == taxLot1SellQuantity && lot.UnitPrice == sellUnitPrice);

        // Check the intermediate tax lots reported in lot transaction states are as expected.
        AssetState IntermediateLotTransactionResultantState = lotStrategyResult.LotTransactions
                                                        .Single(transaction => transaction.LotToSellFrom.Equals(LongTermTaxLot1))
                                                        .ResultantState;
        Assert.DoesNotContain(LongTermTaxLot1, IntermediateLotTransactionResultantState.TaxLots);
        Assert.Contains(TaxLot1, IntermediateLotTransactionResultantState.TaxLots);
        Assert.Contains(TaxLot2, IntermediateLotTransactionResultantState.TaxLots);
        Assert.Contains(TaxLot3, IntermediateLotTransactionResultantState.TaxLots);

        // Check capital gains
        Assert.Equal(shortTermCapitalGains, lotStrategyResult.TotalCapitalGainEvent.ShortTermGain);
        Assert.Equal(longTermCapitalGains, lotStrategyResult.TotalCapitalGainEvent.LongTermGain);
        Assert.Equal(0, lotStrategyResult.TotalCapitalGainEvent.Loss);
    }

    [Fact]
    public void ExecuteFIFOSellWithLoss()
    {
        TaxLot[] taxLots = new TaxLot[] { LongTermTaxLot1, TaxLot1, TaxLot2, TaxLot3 };
        decimal sellUnitPrice = 0.1m;
        decimal totalSellQuantity = taxLots.Sum(lot => lot.Quantity);

        decimal losses = taxLots.Sum(lot => (lot.UnitPrice - sellUnitPrice) * lot.Quantity);

        LotStrategyExecutionResult lotStrategyResult = FIFOLotStrategyHandler.Execute(sellTransactionTime, totalSellQuantity, sellUnitPrice, taxLots);

        Assert.Empty(lotStrategyResult.ResultantState.TaxLots);

        Assert.Equal(taxLots.Length, lotStrategyResult.LotTransactions.Count());
        foreach(TaxLot taxLot in taxLots)
            Assert.Single(lotStrategyResult.LotTransactions, lot =>
                lot.Time == sellTransactionTime && lot.Quantity == taxLot.Quantity && lot.UnitPrice == sellUnitPrice);

        Assert.Equal(0, lotStrategyResult.TotalCapitalGainEvent.ShortTermGain);
        Assert.Equal(0, lotStrategyResult.TotalCapitalGainEvent.LongTermGain);
        Assert.Equal(losses, lotStrategyResult.TotalCapitalGainEvent.Loss);
    }

    [Fact]
    public void ExecuteFIFOSellWithBothLossesAndGains()
    {
        TaxLot[] taxLots = new TaxLot[] { LongTermTaxLot1, TaxLot1, TaxLot2, TaxLot3 };
        decimal sellUnitPrice = 1.1m;
        decimal totalSellQuantity = taxLots.Sum(lot => lot.Quantity);

        decimal shortTermCapitalGains = (sellUnitPrice - TaxLot1.UnitPrice) * TaxLot1.Quantity;
        decimal longTermCapitalGains = (sellUnitPrice - LongTermTaxLot1.UnitPrice) * LongTermTaxLot1.Quantity;
        decimal losses = taxLots.Where(lot => sellUnitPrice < lot.UnitPrice)
                                .Sum(lot => (lot.UnitPrice - sellUnitPrice) * lot.Quantity);

        LotStrategyExecutionResult lotStrategyResult = FIFOLotStrategyHandler.Execute(sellTransactionTime, totalSellQuantity, sellUnitPrice, taxLots);

        Assert.Empty(lotStrategyResult.ResultantState.TaxLots);

        Assert.Equal(taxLots.Length, lotStrategyResult.LotTransactions.Count());

        foreach (TaxLot taxLot in taxLots)
            Assert.Single(lotStrategyResult.LotTransactions,
                lot => lot.Time == sellTransactionTime && lot.Quantity == taxLot.Quantity && lot.UnitPrice == sellUnitPrice);

        Assert.Equal(shortTermCapitalGains, lotStrategyResult.TotalCapitalGainEvent.ShortTermGain);
        Assert.Equal(longTermCapitalGains, lotStrategyResult.TotalCapitalGainEvent.LongTermGain);
        Assert.Equal(losses, lotStrategyResult.TotalCapitalGainEvent.Loss);
    }
}