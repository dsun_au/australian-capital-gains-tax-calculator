﻿using CapitalGainsTax.Core.Models;

namespace CapitalGainsTax.Core.Tests;

static internal class Asset
{
    public readonly static TaxLot LongTermTaxLot1 = new(DateTime.Now.AddYears(-2), 13, 0.78m);

    public readonly static TaxLot TaxLot1 = new(DateTime.Now.AddDays(-3), 3, 1);
    public readonly static TaxLot TaxLot2 = new(DateTime.Now.AddDays(-2), 5, 1.2m);
    public readonly static TaxLot TaxLot3 = new(DateTime.Now.AddDays(-1), 7, 1.4m);
}