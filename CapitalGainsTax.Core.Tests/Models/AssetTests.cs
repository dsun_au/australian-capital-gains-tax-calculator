﻿using Xunit;
using CapitalGainsTax.Core.Models;
using CapitalGainsTax.Core.LotStrategy;

namespace CapitalGainsTax.Core.Models.Tests;

public class AssetTests
{
    private readonly FIFOLotStrategyHandler lotStrategyHandler = new();
    private readonly AssetState startingState = new(DateTimeOffset.Now, Array.Empty<TaxLot>());
    private readonly BuyTransaction buyTransaction;
    private readonly Asset asset;
    public AssetTests()
    {
        buyTransaction = new(DateTimeOffset.Now, 10, 5, startingState, 1);
        asset = new("TestAsset", buyTransaction, lotStrategyHandler);
    }

    [Fact()]
    public void AddTransactionTest()
    {
        BuyTransaction newBuyTransaction = new(DateTimeOffset.Now.AddDays(-3), 2, 3, asset.CurrentState, 1);

        Assert.DoesNotContain(newBuyTransaction, asset.Transactions);
        asset.AddTransaction(newBuyTransaction);
        Assert.Contains(newBuyTransaction, asset.Transactions);
    }

    [Fact()]
    public void ChangeTaxLotStrategyTest()
    {
        FIFOLotStrategyHandler newLotStrategyHandler = new();

        Assert.Same(lotStrategyHandler, asset.LotStrategyHandler);
        asset.ChangeTaxLotStrategy(newLotStrategyHandler);
        Assert.NotSame(lotStrategyHandler, asset.LotStrategyHandler);
    }

    [Fact()]
    public void CurrentStateTest()
    {
        FIFOLotStrategyHandler newLotStrategyHandler = new();

        Assert.Equal(buyTransaction.ResultantState, asset.CurrentState);
    }
}