﻿using CapitalGainsTax.Core.Models;
using static CapitalGainsTax.Core.Tests.Asset;

namespace CapitalGainsTax.Core.Tests.Models;

public class TaxLotTests
{
    [Fact]
    public void IEquatableSameInstance()
    {
        Assert.True(TaxLot1.Equals(TaxLot1));
    }

    [Fact]
    public void IEquatableSameValue()
    {
        TaxLot taxLot = new(TaxLot1.Time, TaxLot1.Quantity, TaxLot1.UnitPrice);

        Assert.True(taxLot.Equals(TaxLot1));
    }

    [Fact]
    public void IEquatableDifferentValue()
    {
        TaxLot taxLot = new(TaxLot1.Time, TaxLot1.Quantity * 2, TaxLot1.UnitPrice);

        Assert.False(taxLot.Equals(TaxLot1));
    }
};