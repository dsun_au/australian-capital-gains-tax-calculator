﻿using CapitalGainsTax.Core.LotStrategy;
using CapitalGainsTax.Core.Models;
using static CapitalGainsTax.Core.Tests.Asset;

namespace CapitalGainsTax.Core.Tests.Models;

public class SellTransactionTests
{
    private static readonly DateTimeOffset tradeTime = DateTimeOffset.Now;
    private const decimal tradeQuantity = 1;
    private const decimal sellUnitPrice = 100;

    private readonly TaxLot taxLot;
    private readonly AssetState assetState;
    private readonly BuyTransaction buyTransaction = new(tradeTime, tradeQuantity, sellUnitPrice,
                                                         new AssetState(DateTimeOffset.Now.AddDays(-1), Array.Empty<TaxLot>()), 10, 1);
    private readonly Core.Models.Asset asset;
    private readonly FIFOLotStrategyHandler lotStrategyHandler = new();

    public SellTransactionTests()
    {

        taxLot = new TaxLot(tradeTime, tradeQuantity, sellUnitPrice);
        assetState = new AssetState(tradeTime, new[]{ taxLot });

        asset = new("TestAsset", buyTransaction, lotStrategyHandler);
    }

    [Fact]
    public void AssetStateTimeCorrect()
    {
        DateTime testTime = DateTime.UtcNow.AddDays(1);
        SellTransaction sellTransaction = new(assetState, asset.LotStrategyHandler, testTime, tradeQuantity, sellUnitPrice);

        Assert.Equal(testTime, sellTransaction.ResultantState?.Time);

    }

    [Fact]
    public void SellAllOfOneLot()
    {
        DateTime testTime = DateTime.UtcNow.AddDays(1);
        SellTransaction sellTransaction = new(assetState, asset.LotStrategyHandler, testTime, tradeQuantity, sellUnitPrice);

        Assert.Equal(0, sellTransaction.ResultantState?.TotalQuantity);
    }

    [Fact]
    public void SellAllOfSeveralLots()
    {
        TaxLot[] taxLots = { TaxLot1, TaxLot2, TaxLot3 };
        decimal totalTradeQuantity = taxLots.Sum(lot => lot.Quantity);

        AssetState assetState1 = new(DateTimeOffset.Now, taxLots);

        SellTransaction sellTransaction = new(assetState1, lotStrategyHandler, DateTime.UtcNow, totalTradeQuantity, sellUnitPrice);

        Assert.Equal(0, sellTransaction.ResultantState?.TotalQuantity);
        Assert.Equal(3, sellTransaction.LotTransactions.Count);
    }

}