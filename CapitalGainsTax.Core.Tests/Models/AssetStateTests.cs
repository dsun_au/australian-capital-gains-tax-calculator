﻿using CapitalGainsTax.Core.Models;
using static CapitalGainsTax.Core.Tests.Asset;

namespace CapitalGainsTax.Core.Tests.Models;

public class AssetStateTests
{

    private readonly AssetState assetState;
    private readonly decimal totalValue;
    private readonly decimal totalQuantity;
    private readonly decimal averagePrice;

    public AssetStateTests()
    {
        assetState = new AssetState(DateTime.Now, new[] { TaxLot1, TaxLot2, TaxLot3 });

        totalValue = TaxLot1.UnitPrice * TaxLot1.Quantity + TaxLot2.UnitPrice * TaxLot2.Quantity + TaxLot3.UnitPrice * TaxLot3.Quantity;
        totalQuantity = TaxLot1.Quantity + TaxLot2.Quantity + TaxLot3.Quantity;
        averagePrice = totalValue / (TaxLot1.Quantity + TaxLot2.Quantity + TaxLot3.Quantity);
    }

    [Fact]
    public void TotalValue()
    {
        Assert.Equal(totalValue, assetState.TotalValue);
    }

    [Fact]
    public void TotalQuantity()
    {
        Assert.Equal(totalQuantity, assetState.TotalQuantity);
    }

    [Fact]
    public void AveragePrice()
    {
        Assert.Equal(averagePrice, assetState.AverageUnitPrice);
    }
}