﻿using CapitalGainsTax.Core.Models;

namespace CapitalGainsTax.Core.Tests.Models;

public class BaseModelTests
{
    private readonly TestModel Model = new TestModel();
    public BaseModelTests()
    {

    }

    [Fact]
    public void BaseModel_PropertyChangedNotification()
    {
        int callCount = 0;

        Model.PropertyChanged += (sender, e) => callCount++;

        Model.TestProperty = "NewString";

        Assert.Equal(1, callCount);
    }

    private class TestModel : ModelBase
    {
        private string testProperty = "TestString";
        public string TestProperty
        {
            get => TestProperty;
            set => SetProperty(ref testProperty, value);
        }
    }
}